import numpy as np
import matplotlib.pyplot as plt

f,ax = plt.subplots(1)

### Constants ###
g       = 9.81
l       = 2.5
omega_0 = 0
theta_0 = np.pi / 36
Omega   = np.sqrt( g / l )
dt  = 0.1
t_f = 10

### Arrays ###
t_n     = [0]
theta_n = [theta_0]
omega_n = [0]


def update_angle(t_n, theta_n, omega_n):

    t_n.append(t_n[-1] + dt )                              # <<==  LINES   ===
    omega_n.append(omega_n[-1] - g / l * theta_n[-1] * dt) # <<==    OF    ===
    theta_n.append(theta_n[-1] + omega_n[-2] * dt)         # <<== INTEREST ===

    print(theta_n[-1], omega_n[-1])

    return True if t_n[-1] > t_f else update_angle(t_n, theta_n, omega_n)

update_angle(t_n, theta_n, omega_n)


plt.title('SHM of a Pendulum', fontsize=18)
plt.xlabel('Time (s)', fontsize=12)
plt.ylabel('Angle (Rad)', fontsize=12)


plt.plot(t_n, theta_n)

plt.show()
